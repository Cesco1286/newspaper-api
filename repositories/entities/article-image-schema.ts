import mongoose from "mongoose";
const Schema = mongoose.Schema;

export let ArticleImageSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    base64content: Buffer,
    fileExtension: String,
    renderPrefix: String, //data:image/jpeg;base64
    imageType: Number,
    articleId: { type: Schema.Types.ObjectId, ref: 'Article' },
    createdAt: Date
})