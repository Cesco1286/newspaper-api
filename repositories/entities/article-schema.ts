import mongoose from "mongoose";
const Schema = mongoose.Schema;

export let ArticleSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    title: String,
    text: String,
    author: String,
    thumbnail: Buffer,
    images: [{ type: Schema.Types.ObjectId, ref: 'ArticleImage' }],
    createdAt: Date
})