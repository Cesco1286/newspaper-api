import mongoose from "mongoose";

import { ArticleSchema } from './entities/article-schema';
import { ArticleImageSchema } from './entities/article-image-schema';

export default class ArticleRepository {

    private _dbConn: any;    
    public _articleModel: any;
    public _articleImageModel: any;

    constructor(dbConnection: any) {
        this._dbConn = dbConnection;
        this._articleModel = this._dbConn.model('Article', ArticleSchema);
        this._articleImageModel = this._dbConn.model('ArticleImage', ArticleImageSchema)
    }

    public async GetLatestArticles(): Promise<Array<any>> {
        let articles = await this._articleModel.find({}).sort('-createdAt').limit(20).exec(); 
        return articles; 
    }

    public async GetArticleImages(articleId: any): Promise<Array<any>>  {
        return await this._articleImageModel.find({ articleId: articleId}).exec();
    }

    public async CreateArticle(article: any) {
        article._id = mongoose.Types.ObjectId();
        await article.save();
        return article;
    }

    public async CreateArticleImage(articleImage: any) {
        articleImage._id = mongoose.Types.ObjectId();
        await articleImage.save();
        return articleImage;
    }

    public async DeleteAll() {
        await this._articleModel.deleteMany({}).exec();
        await this._articleImageModel.deleteMany({}).exec();
    }

}