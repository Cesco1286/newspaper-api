export let getImageBytes = function (imgBase64string: string) {
    let imgBuffer = null;
    if (imgBase64string != null) {
        imgBuffer = Buffer.from(imgBase64string, 'base64');
    }
    return imgBuffer;
};