import ArticleRepository from "../repositories/article-repository";
import { ArticleSchema } from "../repositories/entities/article-schema";
import { ArticleImageSchema } from "../repositories/entities/article-image-schema";
import Article from "../server/models/article";
import ArticleImage from "../server/models/article-image";
import * as imageUtils from "../libs/image-utils";

export default class ArticleManager {

    _articleRepository: ArticleRepository;
    constructor(dbConnection: any) {
        this._articleRepository = new ArticleRepository(dbConnection);
    }

    public async DeleteAll(password: String, configuredPassword: String) {
        console.log("deleting items");
        if (password === configuredPassword) {
            console.log("passwords match");
            await this._articleRepository.DeleteAll();
        } else {
            throw Error("Incorrect passowrd");
        }
    }

    public async createArticle(article: Article) {
        if (article && article != null) {
            let newArticle = new this._articleRepository._articleModel({
                title: article.title,
                text: article.text,
                author: article.author,                
                createdAt: new Date()
            });
            let createdArticle = await this._articleRepository.CreateArticle(newArticle);
            if (article.images && article.images != null && article.images.length > 0 ) {
                for (let image of article.images) {
                    let dbImage = this.getDbImage(createdArticle._id, image);
                    let createdImage = await this._articleRepository.CreateArticleImage(dbImage);
                    createdArticle.images.push(createdImage._id);
                }
                await createdArticle.save();
            }
            return createdArticle;
        } else {
            throw new Error("Article is not well defined");
        }
    }


    public async getLatestArticles() : Promise<Array<Article>> {
        let dbArticles = await this._articleRepository.GetLatestArticles();
        let articles = new Array<Article>();
        for (let article of dbArticles) {
            articles.push(await this.getResponseArticle(article));
        }
        return articles;
    }

    public async getResponseArticle(article: any) {
        var articleImages = await this._articleRepository.GetArticleImages(article._id);
        return new Article(
            article._id,
            article.title,
            article.text,
            article.author,
            (article.thumbnail && article.thumbnail != null) ? Buffer.from(article.thumbnail).toString('base64') : "",
            this.getArticleImages(articleImages)            
        );   
    }    

    //maps a dbImage to a Response Images
    private getArticleImages(dbImages: Array<any>): Array<ArticleImage> {
        var images = new Array<ArticleImage>();
        dbImages.forEach((dbImage: any) => {
            images.push(new ArticleImage(
                dbImage._id,
                dbImage.base64content != null ? Buffer.from(dbImage.base64content).toString('base64') : "",
                dbImage.renderPrefix || "",
                dbImage.imageType
            ));                            
        })
        return images;
    }

    private getDbImage(articleId: any, image: ArticleImage) {
        if (articleId != null && image != null && image.base64content != null && image.base64content.length > 0 ) {
            var imgContentSplit = image.base64content.split(',');
            var base64content = imgContentSplit.length > 1 ? imgContentSplit[1] : imgContentSplit[0];
            var renderPrefix = imgContentSplit.length > 1 ? imgContentSplit[0] : null;
            return new this._articleRepository._articleImageModel({
                base64content: imageUtils.getImageBytes(base64content),                                
                renderPrefix: renderPrefix,
                articleId: articleId,
                imageType: image.imageType,
                createdAt: new Date()
            });
        } else {
            throw Error('The image is not well formed');
        }
    }   
    
}