import ArticleImage from "./article-image";

export default class Article {
    id: any;
    title: string;
    text: string;
    author: string;
    base64Thumbnail: string;
    images: Array<ArticleImage>;

    constructor(
        id: any,
        title: string,
        text: string,
        author: string = "Anonymous",
        base64Thumbnail: string = "",
        images: Array<ArticleImage> = []
    ) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.author = author;
        this.base64Thumbnail = base64Thumbnail;
        this.images = images;
    }
}