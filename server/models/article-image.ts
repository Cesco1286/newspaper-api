export default class ArticleImage {
    id :string;
    base64content :string;
    renderPrefix :string;
    imageType: number; // 0 = thumbnail, 1= normal
    constructor(id: string, base64content: string, renderPrefix: string, imageType: number) {
        this.id = id;
        this.base64content = base64content;
        this.renderPrefix = renderPrefix;
        this.imageType = imageType; 
    }
}