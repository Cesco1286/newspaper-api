import Article from "../models/article";

export default class GetArticleResponse {
    articles: Array<Article>;
    constructor(articles: Array<Article>) {
        this.articles = articles;
    }
}