import { Request, Response } from "express";
import mongoose from "mongoose";
import "reflect-metadata";
import { plainToClass, deserialize } from "class-transformer";
import DeleteArticleRequest from "../requests/delete-articles-request";

import GetArticleResponse from '../responses/getArticleResponse';
import Article from '../models/article';
import ArticleManager from "../../business/article-manager";


const _dbConnection = mongoose.createConnection(process.env.DBCONN || "mongodb://timetracker:princo11@ds233895.mlab.com:33895/cesco?retryWrites=false" ); 
const _deletePassword = process.env.DELETEALL_PASSWORD || "pasaje1234";
const _articleManager = new ArticleManager(_dbConnection);

export let latestArticles = async (req: Request, res: Response) => {
    console.log("loading the articles");
    let articles = await _articleManager.getLatestArticles();
    res.json(new GetArticleResponse(articles));    
};

export let createArticle = async (req: Request, res: Response) => {    
    console.log("********ENTERED INSIDE CREATE ARTICLE**********");
    let article = deserialize(Article, JSON.stringify(req.body));
    let createdArticle = await _articleManager.createArticle(article);
    res.json({ createdArticle });
}

export let deleteArticles = async (req: Request, res: Response) => {
    console.log("entered in deletearticles");
    let deleteRequest: DeleteArticleRequest;
    deleteRequest = deserialize(DeleteArticleRequest, JSON.stringify(req.body));

    try {
        await _articleManager.DeleteAll(deleteRequest.password, _deletePassword);
        res.json({ success: "true" });
    } catch (err) {
        res.json({ err });
    }
} 