import express from "express";
import bodyParser from "body-parser";
import { Request, Response } from "express";

import ArticleRepository from "../repositories/article-repository";
import * as articleSchema from "../repositories/entities/article-schema";
import * as articleImageSchema from "../repositories/entities/article-image-schema";
import * as articleController from "./controllers/article-controller";


const server = express();

server.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
server.use(bodyParser.json({ limit: '50mb' }));
server.use(function (req: any, res: any, next: any) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



server.get('/articles/latest', articleController.latestArticles);
server.post('/article', articleController.createArticle);
server.post('/articles/deleteall', articleController.deleteArticles);

console.log(process.env.PORT || "env port null");
server.listen(process.env.PORT || 8000, () => { console.log("listening at 8000") })